#!/bin/python3
# Candidate universal permutation cycle
cycle = [(22),(23),(24),(21),(20),(18),(19),3,(17),4,2,(16),1,5,6,7,(11),(10),8,(13),9,(12),(15),(14)]
# Candidate universal permutation word
word = cycle.copy()
word.extend(cycle[0:3])

def reduce(permutation):
    nums = permutation.copy()
    nums.sort()
    return [nums.index(i) + 1 for i in permutation]

reduced_permutations = [tuple(reduce(word[i:i+4])) for i in range(len(cycle))]

for i,p in enumerate(reduced_permutations):
    if (i2 := reduced_permutations.index(p)) != i:
        print(f"Reduced permutation {p} at position {i+1} already appeared at position {i2+1}")
